    Il existe deux types de composants en React, les composants de classes et de fonctions. Il est recommandé d’utiliser les composants de fonctions.

    Un composant React peut prendre en entrée une ou plusieurs props, qui sont l’équivalent des attributs en HTML.

    Un composant React retourne une portion de DOM virtuel.

    Nous décrivons notre DOM virtuel avec la syntaxe JSX, car c’est la méthode la plus recommandée.

    Les Hooks permettent de palier aux limites aux composants de fonction, en leur ajoutant des fonctionnalités supplémentaires.

    Le Hook d’état permet de définir un state à nos composants.

    Le Hook d’effet permet d’intervenir sur le cycle de vie de nos composants.


regle 1 :
ne jamais utiliser un hook dans une boucle ou une condition. 
les hooks doivent etre appeler uniquemlent au niveau de la racine

regle 2 :
appeler les hooks uniquement depuis les composants de fonction
on peut toutefois appeler un hook depuis un autre hook (voir hook personnalisé)

regele 3 :
modifier un état par remplacement
par exemple, on n'ajoute pas un élément dans un tableau. on crée un nouveau tableau qui remplace le précédent dans l'etat du composant

---------------------------------------------
la librairie react router simule la navigation dans le projet react

----------------------------------------------
API REST simulation with :
npm install -g json-server
command to run json-server :
json-server --watch src/models/db.json --port=3001
see it on http://localhost:3001/pokemons


-------------------------------------------------------
12 pokemons for db

{
  "pokemons": [
    {
      "id": 1,
      "name": "Bulbizarre",
      "hp": 25,
      "cp": 5,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
      "types": ["Plante", "Poison"]
     },
     {
      "id": 2,
      "name": "Salamèche",
      "hp": 28,
      "cp": 6,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/004.png",
      "types": ["Feu"]
     },
     {
      "id": 3,
      "name": "Carapuce",
      "hp": 21,
      "cp": 4,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/007.png",
      "types": ["Eau"]
     },
     {
      "id": 4,
      "name": "Aspicot",
      "hp": 16,
      "cp": 2,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/013.png",
      "types": ["Insecte", "Poison"]
     },
     {
      "id": 5,
      "name": "Roucool",
      "hp": 30,
      "cp": 7,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/016.png",
      "types": ["Normal", "Vol"]
     },
     {
      "id": 6,
      "name": "Rattata",
      "hp": 18,
      "cp": 6,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/019.png",
      "types": ["Normal"]
     },
     {
      "id": 7,
      "name": "Piafabec",
      "hp": 14,
      "cp": 5,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/021.png",
      "types": ["Normal", "Vol"]
     },
     {
      "id": 8,
      "name": "Abo",
      "hp": 16,
      "cp": 4,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/023.png",
      "types": ["Poison"]
     },
     {
      "id": 9,
      "name": "Pikachu",
      "hp": 21,
      "cp": 7,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/025.png",
      "types": ["Electrik"]
     },
     {
      "id": 10,
      "name": "Sabelette",
      "hp": 19,
      "cp": 3,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/027.png",
      "types": ["Normal"]
     },
     {
      "id": 11,
      "name": "Mélofée",
      "hp": 25,
      "cp": 5,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/035.png",
      "types": ["Fée"]
     },
     {
      "id": 12,
      "name": "Groupix",
      "hp": 17,
      "cp": 8,
      "picture": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/037.png",
      "types": ["Feu"]
     }
  ]
}

---------  deploy -------------------------------------------------------------
peparer pokemon-services en definissant son fonctionnement pour la production
npm run build  : pour preparer l'app 
le dossier build qui contient le livrable est créé.

pour tester le livrable comme sur un serveur distant installer:
npm install -g serve
lancer le server build 
serve -s build

creation du prjet sur firebase
installer la boite à outils firebase
npm install -g firebase-tools
firebase --version
firebase login    firebase logout if needed
firebase init  follow the proceed in shell
firebase deploy 

 https://console.firebase.google.com/project/react-pokemon-app-d6422/overview
 https://react-pokemon-app-d6422.web.app 



